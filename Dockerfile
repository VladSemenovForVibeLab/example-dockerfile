FROM openjdk:17.0.2-jdk-slim-buster
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

# сборка проекта
# mvn clean package
# docker build -t upagge/spring-boot-docker:dockerfile .
# dive upagge/spring-boot-docker:dockerfile

